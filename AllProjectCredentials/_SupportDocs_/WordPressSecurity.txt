http://kb.siteground.com/basic_security_guidelines_for_the_shared_hosting_server/

1. Pick up strong passwords for the main cPanel account, MySQL, FTP and mail users. Never use the same passwords for different users. For example a MySQL user should not have the same password as your cPanel user or an FTP user. It is essential that your cPanel user's password is not found in any file on your account by any means;

2. Check whether all of your web applications are up-to-date. This includes any modules, components and addons you have added and / or integrated;

3. Restrict admin access to only required users
    Administrator � can do everything
    Editor � They write stuff, more importantly they can publish
    Contributor � The Author�s cousin, but they can�t publish
    Subscriber � They follow your rants about your favorite past times but can�t do anything else


4. Avoid having directories with permissions above 755. If your applications require such directories, try to put them outside your webroot (public_html) or place a .htaccess file in them containing "deny from all" to restrict public access to these files.

5. Tweak your local PHP settings for better security. This can be done by disabling unnecessary functions and options. Here are some sample recommended directives:
allow_url_fopen=off
disable_functions = proc_open , popen, disk_free_space, set_time_limit, leak, tmpfile, exec, system, shell_exec, passthru
Note that the above directives can cripple your code's functionality. They have to be pasted in a php.ini file in each directory you'd like to have them applied.

6. If you are not using Perl scripts, add a bogus handler for these files. In your home directory create a .htaccess file with the following content:
##Deny access to all CGI, Perl, Python and text files
<FilesMatch ".(cgi|pl|py|txt)">
Deny from all
</FilesMatch>
##If you are using a robots.txt file, please remove the
# sign from the following 3 lines to allow access only to the robots.txt file:
#<FilesMatch robots.txt>
#Allow from all
#</FilesMatch>
The above will prevent Perl scripts from being executed. Many exploits / backdoors are writtent in Perl and the above will prevent them from running. This directive will apply to all your subdirectories.


http://blog.sucuri.net/2012/07/wordpress-and-server-hardening-taking-security-to-another-level.html

7. WordPress has many files, but very few of them need to be accessed directly by the users. To prevent files being accessed directly, we recommend blocking direct PHP access to /wp-content with the following additions to your .htaccess file:
    <Files *.php>
    deny from all
    </Files> 

8. Correcting Application Access Control
The admin panel (wp-admin) runs on the same domain and same privileges as the rest of the application. In a perfect world, it would be isolated, but to minimize issues, we recommend adding two restrictions to wp-admin access (via .htaccess):
8.1- Only allowing certain IP addresses:
    order allow, deny
    allow from IP1
    allow from IP2
    deny from all 
8.2- Adding an additional user/pass (pre-authentication):
    AuthUserFile /var/www/mysite/.htpasswd
    AuthName �Site private access�
    AuthType Basic
    require user myuser2
By adding those two restrictions, you isolate wp-admin, requiring additional permissions and again, improve the security model. Hope you�re starting to get the trend of the post.


