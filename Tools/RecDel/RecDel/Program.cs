﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Configuration;


namespace RecDel
{
    class Program
    {
        static void Main(string[] args)
        {
            string strRootDir = System.Configuration.ConfigurationSettings.AppSettings.Get("RootDir");
            int intDaysOld = Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings.Get("DaysOld"));

            foreach (FileInfo fi in new DirectoryInfo(strRootDir).GetFiles("*.*",SearchOption.AllDirectories))
            {
                if (fi.LastWriteTime.Date < DateTime.Now.Subtract(new TimeSpan(intDaysOld, 0, 0, 0)))
                {
                    try
                    {

                        fi.Delete();
                    }
                    catch { }
                }
            }

            if (System.Configuration.ConfigurationSettings.AppSettings.Get("DeleteSubDir") == "true")
            {
                foreach (DirectoryInfo di in new DirectoryInfo(strRootDir).GetDirectories())
                {
                    if (di.LastWriteTime.Date < DateTime.Now.Subtract(new TimeSpan(intDaysOld, 0, 0, 0)))
                    {
                        try
                        {
                            if (di.GetFiles().Length == 0)
                            {
                                di.Delete();
                            }
                        }
                        catch { }
                    }
                }
            }
        }
    }
}
