using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.TeamFoundation;
using Microsoft.TeamFoundation.Common;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.Client;


namespace TFSFileBackup
{
    class Program
    {
        string transLogFilePath = "";
        string errLogFilePath = "";

        static void Main(string[] args)
        {
            new Program().GetLatestFiles();
        }

        private Program()
        {
            transLogFilePath = ConfigurationSettings.AppSettings.Get("TransactionLogFile") + "_" + DateTime.Now.ToString("MMMddyyyy") + ".txt";
            errLogFilePath = ConfigurationSettings.AppSettings.Get("ErrorLogFile") + "_" + DateTime.Now.ToString("MMMddyyyy") + ".txt";
        }

        private void GetLatestFiles()
        {
            try
            {
                TeamFoundationServer tfs = new TeamFoundationServer("http://daltfsapp:8080/", new System.Net.NetworkCredential("daltfsadmin", "$$tf$9876"));
                tfs.EnsureAuthenticated();

                Workspace workspace = null;

                // Connect to TFS Source Control                
                VersionControlServer vcs = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));
                //DataSet ds = TextToDataSet.Convert(ConfigurationSettings.AppSettings.Get("BackupCandidiates"), "ActiveProjects", "");
                DataSet ds = SqlHelper.ExecuteDataset(ConfigurationSettings.AppSettings.Get("DSN"), "sproc_DALGetCodeBackupCandidates", Convert.ToInt32(ConfigurationSettings.AppSettings.Get("BackupDuration_Days")));
                DataTable dt = ds.Tables[0];

                //Add Exception Project
                string[] strArrExceptions = ConfigurationSettings.AppSettings.Get("ExceptionProjects").Split(',');
                for (int i = 0; i < strArrExceptions.Length; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = strArrExceptions[i];
                    dt.Rows.Add(dr);
                }

                string[] strArrSkips = ConfigurationSettings.AppSettings.Get("SkipProjects").Split(',');
                for (int i = 0; i < strArrSkips.Length; i++)
                {
                    DataRow[] drs = dt.Select("ProjectName='" + strArrSkips[i] + "'");

                    for (int j = 0; j < drs.Length; j++)
                    {
                        drs[j].Delete();
                    }
                }
                dt.AcceptChanges();

                int intRowCount = dt.Rows.Count;
                for (int i = 0; i < intRowCount; i++)
                {
                    try
                    {
                        DataRow dr = dt.Rows[i];
                        // Create a Workspace                
                        string repository = "$/" + dr[0].ToString() + "/";
                        repository = repository.Replace("\\", "/");
                        // location to check out files to:                
                        string workspacePath = ConfigurationSettings.AppSettings.Get("RootWorkspacePath") + dr[0].ToString();
                        workspace = vcs.CreateWorkspace("TFSCodeBackupWorkSpace", vcs.AuthenticatedUser);
                        WorkingFolder folder = new WorkingFolder(repository, workspacePath);
                        // Get Files                
                        workspace.CreateMapping(folder);
                        Console.WriteLine("Getting files for " + dr[0].ToString() + "...");
                        workspace.Get();
                        if (!Directory.Exists(workspacePath))
                        {
                            Console.WriteLine("Failed..." + repository + " does not exist");
                            LogToFile(repository);
                        }
                        Console.WriteLine("Get Latest Operation Completed for " + dr[0].ToString() + " !!!");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                        // clean up the workspace for subsequent passes.                
                        if (workspace != null)
                            workspace.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogError(ex.ToString());
            }
            finally
            {
                Console.Beep(50, 1000);
                //Console.WriteLine("Batch Completed.\\r\\nPress any key to exit...");
                //Console.ReadKey();

            }
        }

        protected void LogToFile(string strMessage)
        {
            StreamWriter sw = new StreamWriter(transLogFilePath, true);

            try
            {
                sw.WriteLine(strMessage);
            }
            catch (Exception)
            {
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        protected void LogError(string strMessage)
        {
            StreamWriter sw = new StreamWriter(errLogFilePath, true);

            try
            {
                sw.WriteLine("====================BEGIN ERROR================================");
                sw.WriteLine(strMessage);
                sw.WriteLine("====================END ERROR==================================");
            }
            catch (Exception)
            {
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

    }
}
