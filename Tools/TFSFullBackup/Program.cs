using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.TeamFoundation;
using Microsoft.TeamFoundation.Common;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.IO;

namespace TFSFullBackup
{
    class Program
    {
        string transLogFilePath = "";
        string errLogFilePath = "";

        static void Main(string[] args)
        {
            new Program().GetLatestFiles();
        }

        private Program()
        {
            transLogFilePath = ConfigurationSettings.AppSettings.Get("TransactionLogFile") + "_" + DateTime.Now.ToString("MMMddyyyy") + ".txt";
            errLogFilePath = ConfigurationSettings.AppSettings.Get("ErrorLogFile") + "_" + DateTime.Now.ToString("MMMddyyyy") + ".txt";
        }

        private void GetLatestFiles()
        {
            try
            {

                TeamFoundationServer tfs = new TeamFoundationServer("http://daltfsapp:8080/", new System.Net.NetworkCredential("daltfsadmin", "$$tf$"));
                tfs.EnsureAuthenticated();

                // Connect to TFS Source Control                
                VersionControlServer vcs = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));

                WorkItemStore objWorkItemStore = (WorkItemStore)tfs.GetService(typeof(WorkItemStore));

                Workspace workspace = null;

                foreach (Project pr in objWorkItemStore.Projects)
                {
                    try
                    {
                        if (Convert.ToInt32(Convert.ToChar(pr.Name.ToLower().Substring(0, 1))) >= 108)
                        {

                            // Create a Workspace                
                            string repository = "$/" + pr.Name + "/";
                            repository = repository.Replace("\\", "/");
                            // location to check out files to:                
                            string workspacePath = ConfigurationSettings.AppSettings.Get("RootWorkspacePath") + pr.Name;
                            workspace = vcs.CreateWorkspace("TFSFullBackupWorkSpace", vcs.AuthenticatedUser);
                            WorkingFolder folder = new WorkingFolder(repository, workspacePath);

                            // Get Files                
                            workspace.CreateMapping(folder);
                            Console.WriteLine("Getting files for " + pr.Name + "...");
                            workspace.Get();
                            if (!Directory.Exists(workspacePath))
                            {
                                Console.WriteLine("Failed..." + repository + " does not exist");
                                LogToFile(repository);
                            }
                            Console.WriteLine("Get Latest Operation Completed for " + pr.Name + " !!!");

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                        // clean up the workspace for subsequent passes.                
                        if (workspace != null)
                            workspace.Delete();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogError(ex.ToString());
            }
            finally
            {
                Console.Beep(50, 1000);
                //Console.WriteLine("Batch Completed.\\r\\nPress any key to exit...");
                //Console.ReadKey();

            }
        }

        protected void LogToFile(string strMessage)
        {
            StreamWriter sw = new StreamWriter(transLogFilePath + "_" + DateTime.Now.ToShortDateString(), true);

            try
            {
                sw.WriteLine(strMessage);
            }
            catch (Exception)
            {
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        protected void LogError(string strMessage)
        {
            StreamWriter sw = new StreamWriter(errLogFilePath + "_" + DateTime.Now.ToShortDateString(), true);

            try
            {
                sw.WriteLine("====================BEGIN ERROR================================");
                sw.WriteLine(strMessage);
                sw.WriteLine("====================END ERROR==================================");
            }
            catch (Exception)
            {
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }
    }
}
