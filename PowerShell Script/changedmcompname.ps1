$domain = "digital"
$user = "administrator"
$username = "$domain\$user"
$password = Read-Host -Prompt "Enter password for $username" -AsSecureString
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
$computeroldname = Read-Host -Prompt "Enter the computer's current name"
$computernewname = Read-Host -Prompt "Enter the computer's NEW name"
$computername.Rename �computername $computeroldname �newname $computernewname �domaincredential $credential �force �restart
