$username = "digital\administrator"
$password = Read-Host -Prompt "Enter password for $username" -AsSecureString
$myCred = New-Object System.Management.Automation.PSCredential $username, $password
Remove-Computer -Credential $myCred -Force -PassThru –force –restart
