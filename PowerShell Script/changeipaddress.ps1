$wmi = Get-WmiObject win32_networkadapterconfiguration -filter "ipenabled = 'true'"
$address = Read-Host -Prompt "Please Enter the New IP for this System."
$wmi.EnableStatic($address, "255.255.0.0")
$wmi.SetGateways("192.168.1.200", 1)
$wmi.SetDNSServerSearchOrder("192.168.1.3,192.168.1.4")