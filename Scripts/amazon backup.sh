NOW=$(date +"%m-%d-%Y");
DOM=$(date +"%d");
DOW=$(date +"%u");
if [ "$DOM" = 1 ]; then
	mysqldump -u 'da-systems' -h api.prime.sentientdecisionscience.com --password=hOvIre5iqu 'api-prime' | gzip > /var/databasebackup/apiprime$NOW.sql.gz;
	s3cmd put /var/databasebackup/apiprime$NOW.sql.gz s3://SentientPrime/Database_Backup/MonthlyBackup/;
	exit;
elif [ "$DOM" -gt 1 -a "$DOW" = 0 ]; then
	mysqldump -u 'da-systems' -h api.prime.sentientdecisionscience.com --password=hOvIre5iqu 'api-prime' | gzip > /var/databasebackup/apiprime$NOW.sql.gz;
	s3cmd put /var/databasebackup/apiprime$NOW.sql.gz s3://SentientPrime/Database_Backup/WeeklyBackup/;
	exit;
else
	mysqldump -u 'da-systems' -h api.prime.sentientdecisionscience.com --password=hOvIre5iqu 'api-prime' | gzip > /var/databasebackup/apiprime$NOW.sql.gz;
	s3cmd put /var/databasebackup/apiprime$NOW.sql.gz s3://SentientPrime/Database_Backup/DailyBackup/;
	exit;
fi